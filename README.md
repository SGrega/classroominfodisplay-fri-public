 
# Quick summary #
ClassroomInfoDisplay is a small internal faculty (Faculty of Computer and Information Science, University of Ljubljana) project to push important information to students and teachers via above our classrooms displays.  
 
# Features #
- works in offline mode in case of server or WiFi outage/error (data sync management) 
- data transfer optimization (based on data priorities and pre-caching of not time critical data) 
- auto boot-up in case of electrical outage/error (custom Android configuration) 
- auto text resize (based on text length and its' predetermined space size and position)
- ... 
 
# Code #
Mainly developed with HTML/CSS/JavaScript (and different frameworks - JQuery,..), but also using PHP for some internal reasons.  
Also using Apache for serving web pages and reconfigured Android HDMI TV sticks with WebView technology which are connected to above classrooms displays. 
 
 
# Data #
I used internal time reservation system for our classrooms (via REST API) and the University LDAP (via PHP requests) 
 
 
# Version #
Version 1.0 public - to demonstrate daily timetable of internal and external reservations for specific classroom. 
 
Future planes are to push device specific push notifications in case of special events.
 

# Pictures of deployed devices #

![prototip photo1] (https://drive.google.com/uc?id=0B4czODcUchVmdzlILTNjVjIyZ1E&export=download)

![prototip photo2] (https://drive.google.com/uc?id=0B4czODcUchVmSlVMTjIxM0tYSTg&export=download)

![prototip photo3] (https://drive.google.com/uc?id=0B4czODcUchVmOVN2eGpVQjBoNzg&export=download)


# Contribution guidelines #

All the code, UI, server configuration, Android configurations or modifications are developed by myself.
Big thanks to the faculty mentor Lect. MSc Matej Grom (IT Support Services at the faculty) and his internal team for their free hands at deploying displays, Android TV devices and testing.

Licence: opensource MIT ( https://opensource.org/licenses/MIT )


# Who do I talk to? #

- Repo owner or admin
- Other community or team contact

(The code will be published soon, I need a little time to clean up all internal things, meantime you can contact me and  hopefully I will provide you the code)